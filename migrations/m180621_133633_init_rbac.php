<?php

use yii\db\Migration;

/**
 * Class m180621_133633_init_rbac
 */
class m180621_133633_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
 $auth = Yii::$app->authManager;

 ///////////הגדרת התפקידים /////////////

    $admin = $auth->createRole('admin');
      $auth->add($admin);
              
      $teamleader = $auth->createRole('teamleader');
      $auth->add($teamleader);

      $member = $auth->createRole('member');
      $auth->add($member);

      $manager = $auth->createRole('manager');
      $auth->add($manager);

////////////////הגדרת ירושות///////////
      $auth->addChild($admin, $teamleader);
      $auth->addChild($teamleader, $member);
      $auth->addChild($member, $manager);

 //////////////// הגדרת ההרשאות //////////////////
      $manageUsers = $auth->createPermission('manageUsers');
      $auth->add($manageUsers);

     $updateLevel = $auth->createPermission('updateLevel');
      $auth->add($updateLevel); 

      $manageBreakdown = $auth->createPermission('manageBreakdown');
      $auth->add($manageBreakdown);   
              
      $managerview = $auth->createPermission('managerview');
      $auth->add($managerview);

      $vieweOwnditails = $auth->createPermission('vieweOwnditails'); ////צפיה בפרטים של עצמך בלבד
      $rule = new \app\rbac\MemberRule;
      $auth->add($rule);
      $vieweOwnditails->ruleName = $rule->name;                
      $auth->add($vieweOwnditails);                 
        

//////////////////////קביעה מי יכול ליישם איזה חוק
      $auth->addChild($admin, $manageUsers);
      $auth->addChild($teamleader, $updateLevel);
      $auth->addChild($member, $manageBreakdown); 
      $auth->addChild($member, $managerview); 
      $auth->addChild($vieweOwnditails, $manageBreakdown);   

////////////////////// הגדרת המשתמשים הרשומים המערכת ()
$auth->assign($admin,1);
$auth->assign($teamleader,2);
$auth->assign($member,3);
$auth->assign($manager,4);
$auth->assign($admin,5);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180621_133633_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180621_133633_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
